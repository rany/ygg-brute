/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#include <cstdint>

struct Result {
    uint8_t skey[32];
    char address[46]; // the limit is from POSIX
};
