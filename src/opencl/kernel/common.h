/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned uint32_t;
typedef unsigned long uint64_t;

#define DEVICE
#define KERNEL __kernel
#define GLOBAL __global
#define LOCAL  __local
#define SYNCTHREADS() barrier(CLK_LOCAL_MEM_FENCE)

inline size_t global_id()
{
    return get_global_id(0);
}

inline size_t local_id()
{
    return get_local_id(0);
}

inline size_t global_size()
{
    return get_global_size(0);
}

inline size_t local_size()
{
    return get_local_size(0);
}

inline uint32_t bswap_u32(uint32_t src)
{
    union {
        uchar4 u4;
        uint32_t u32;
    } tmp1, tmp2;

    tmp1.u32 = src;
    tmp2.u4 = tmp1.u4.wzyx;
    return tmp2.u32;
}