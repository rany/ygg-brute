/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#include <cstdint>
#include <cstddef>

struct GeneratorParams {
    int device{0};

    uint64_t seed = 0;
    uint64_t seq = 0;

    size_t block_size = 0;
    size_t n_blocks = 0;
    size_t inv_batch_size = 0;
};

class GeneratorCtx {
public:
    ~GeneratorCtx() = default;
    virtual size_t batch_size() = 0;
    virtual const uint8_t* address(size_t i) = 0;
    virtual const uint8_t* skey(size_t i) = 0;
};

class Generator {
public:
    virtual ~Generator() = default;
    virtual void produce(GeneratorCtx& ctx) = 0;
};