/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#include <iostream>
#include <iomanip>

void out_hex_str(std::ostream& os, const uint8_t* data, size_t size) {
    auto hex_nibble = [](uint8_t n) -> char {
        if(n < 10)
            return '0' + n;
        return 'a' + n - 10;
    };

    for(size_t i = 0; i < size; ++i) {
        uint8_t c = data[i];
        os << hex_nibble(c / 16) << hex_nibble(c % 16);
    }
}
