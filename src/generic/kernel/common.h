/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#define col_load(DST, SRC, ROWS, COLS, N)         \
    do                                            \
    {                                             \
        for (size_t _i = 0; _i < (ROWS); ++_i)    \
            (DST)[_i] = (SRC)[_i * (COLS) + (N)]; \
    } while (0)

#define col_store(DST, ROWS, COLS, SRC, N)        \
    do                                            \
    {                                             \
        for (size_t _i = 0; _i < (ROWS); ++_i)    \
            (DST)[_i * (COLS) + (N)] = (SRC)[_i]; \
    } while (0)

#define fe_load(DST, SRC, N, I) col_load(DST, SRC, FE_SIZE, N, I)
#define fe_store(DST, N, SRC, I) col_store(DST, FE_SIZE, N, SRC, I)

#define fe_load_global_idx(DST, SRC, I) fe_load(DST, SRC, global_size(), I)
#define fe_load_global(DST, SRC) fe_load_global_idx(DST, SRC, global_id())

#define fe_store_global_idx(DST, SRC, I) fe_store(DST, global_size(), SRC, I)
#define fe_store_global(DST, SRC) fe_store_global_idx(DST, SRC, global_id())