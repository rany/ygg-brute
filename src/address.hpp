/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#include <cstdint>
#include <cstring>

#ifdef __linux__

#include <arpa/inet.h>
#include <netinet/in.h>

#endif

inline void ipv6_to_string(const uint8_t src[16], char dst[46])
{
    // TODO : raw pointer cast ?
    // TODO : check error ?
    struct in6_addr addr;
    memcpy(addr.s6_addr, src, 16);
    inet_ntop(AF_INET6, &addr, dst, 46);
}
