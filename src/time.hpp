/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#include <chrono>

class TimeInterval
{
public:
    using clock_t = std::chrono::steady_clock;
    using time_point_t = typename clock_t::time_point;

    TimeInterval(time_point_t begin, time_point_t end)
    : begin_{begin}
    , end_{end}
    {}

    double us() const { return std::chrono::duration_cast<std::chrono::microseconds>(end_ - begin_).count(); }
    double s() const { return us() / 1000000; }

private:
    const time_point_t begin_;
    const time_point_t end_;
};

class Timer {
public:
    Timer()
    : start_ts_{clock_t::now()}
    {}

    TimeInterval measure()
    {
       return TimeInterval{start_ts_, clock_t::now()};
    }

private:
    using clock_t = std::chrono::steady_clock;
    const clock_t::time_point start_ts_;
};
