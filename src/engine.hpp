/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#include <memory>
#include <string>

#include "generator.hpp"

class Engine {
public:
    virtual ~Engine() = default;

    virtual const char* name() = 0;
    virtual void print_info() = 0;
    virtual std::string device_name(size_t i) = 0;

    virtual void fill_params(GeneratorParams& params) = 0;
    virtual std::unique_ptr<Generator> make_generator(const GeneratorParams& params) = 0;
    virtual std::unique_ptr<GeneratorCtx> make_generator_context() = 0;
};