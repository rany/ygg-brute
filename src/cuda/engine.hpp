/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#include "../engine.hpp"

namespace cuda {

Engine& get_engine();

} // namespace cuda
