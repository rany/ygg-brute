/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#pragma once

#include <algorithm>
#include <cstdint>

struct Stats {
    uint64_t processed{0};
    uint64_t found{0};
    uint64_t max{0};
    uint64_t unique{0};

    void merge(const Stats& s) {
        processed += s.processed;
        found += s.found;
        max = std::max(max, s.max);
        unique = s.unique;
    }
};