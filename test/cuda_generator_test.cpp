/*
*  This file is part of ygg-brute
*  Copyright (c) 2020 ygg-brute authors
*  See LICENSE for licensing information
*/

#include "generator_test.hpp"

#include "cuda/engine.hpp"

int main()
{
    GeneratorParams params;
    params.seed = 1111;
    params.seq = 2222;
    params.n_blocks = 256;
    params.inv_batch_size = 256;

    cuda::get_engine().fill_params(params);
    auto gen = cuda::get_engine().make_generator(params);
    auto ctx = cuda::get_engine().make_generator_context();
    for(auto i = 0; i < 3; ++i) {
        run_generator_test(*gen, *ctx);
    }

    return 0;
}