# ygg-brute

A CUDA miner for yggdrasil network addresses

## Warning: this is an alpha version software. Bugs are to be expected.

# Build

The GPU's compute capability should be specified for optimal performance.
It can be obtained from [CUDA capabilities](https://developer.nvidia.com/cuda-gpus).

```bash
cmake -H. -Bbuild -DCMAKE_CUDA_ARCHITECTURES=<your CUDA capability, i.e 61 for Pascal 1060 GTX>
cmake --build build
```

# Usage

```bash
ygg-brute --help
```